# Knapsack

Program provádí asymetrický šifrovací algoritmus, založený na principu zavazadlového algoritmu. Program očekává při spuštění čtyři argumenty: vstupní souborm který se bude šifrovat (input.txt), parametry p a q (např. p=77, q=1993) a cestu k souboru s privátním klíčem (private_key.txt). Program v první fázi načte soukromý klíč a vytvoří z něj klíč veřejný. Druhou fází programu je šifrování. Program pro hledání multiplikativní inverzní hodnoty parametru p používá rozšířený Eukleidův algoritmus.
