import java.io.*;
import java.util.Scanner;

public class Knapsack {

    private static int p;
    private static int q;

    public static void main(String[] args) throws IOException {
        String input;
        String private_keys;
        int[] keys;
        int[] public_keys;
        int[] encrypted;
        int[] decryptedDec;
        String decrypted;

        if (args.length != 4) {
            System.err.println("Wrong number of parametrs!");
            System.exit(1);
        }

        input = args[0];
        p = Integer.parseInt(args[1]);
        q = Integer.parseInt(args[2]);
        private_keys = args[3];

        if (!private_keys.contains(".txt")) {
            System.err.println("Wrong file with private keys!");
            System.exit(1);
        }

        keys = loadKeys(private_keys);
        public_keys = firstPhase(keys, p, q);

        encrypted = secondPhase(public_keys, input);
        decryptedDec = decrypt(encrypted, p, q);
        decrypted = decrChar(keys, decryptedDec);
        saveKey(encrypted, "ciphertext.txt");
        saveKey(decryptedDec, "private.txt");
        saveDecr(decrypted, "deciphered.txt");

        System.out.println("All files saved successfully.");
    }

    private static void saveDecr(String dec, String s) {
        try {
            FileWriter myWriter = new FileWriter(s);
            myWriter.write(dec);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private static String decrChar(int[] keys, int[] decryptedDec) {
        String decr = "";
        String temp = "";
        int value;

        for (int j : decryptedDec) {
            value = j;
            for (int x = keys.length - 1; x >= 0; --x) {
                if (value >= keys[x]) {
                    temp = "1" + temp;
                    value -= keys[x];
                } else
                    temp = "0" + temp;
            }
            decr += binToDec(temp);
            temp = "";
        }

        return decr;
    }

    private static String binToDec(String decr) {
        int count = 0;
        int val = 1;
        char data;

        for (int i = decr.length() - 1; i >= 0; --i) {
            count += ((decr.charAt(i) - 48) * val);
            val *= 2;
        }
        data = (char) (count);

        return String.valueOf(data);
    }

    private static int[] decrypt(int[] encrypted, int p, int q) {
        int invP;
        int[] decr;

        invP = getInvP(p, q);
        decr = decryption(encrypted, invP, q);

        return decr;
    }

    private static int[] decryption(int[] encrypted, int invP, int q) {
        int[] dec = new int[encrypted.length];

        for (int i = 0; i < encrypted.length; ++i) {
            dec[i] = (encrypted[i] * invP) % q;
        }
        return dec;
    }

    private static int getInvP(int p, int q) {
        int invP;
        invP = extendedEukleid(p, q, 1)[1];

        while (invP < 0)
            invP += q;

        return invP;
    }

    private static int[] secondPhase(int[] public_keys, String input) {
        String binInput;
        String inpt;
        int[] encr;

        inpt = loadInput(input);
        binInput = toBin(inpt);
        encr = encrypt(binInput, public_keys);

        return encr;
    }

    private static int[] encrypt(String binInput, int[] pub_key) {
        int[] ciphr = new int[binInput.length() / pub_key.length];
        int count = 0;

        while (binInput.length() % pub_key.length != 0)
            binInput = binInput + "0";

        for (int i = 0; i < ciphr.length; ++i) {
            for (int x = 0; x < pub_key.length; ++x) {
                if (binInput.charAt(x + pub_key.length * i) == '1')
                    count += pub_key[x];
            }
            ciphr[i] = count;
            count = 0;
        }
        return ciphr;
    }

    private static String loadInput(String input) {
        String s = "";

        try {
            File myObj = new File(input);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                s += myReader.nextLine() + "\n";
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return s;
    }

    private static String toBin(String input) {
        String temp;
        String s = "";
        for (int i = 0; i < input.length(); ++i) {
            temp = Integer.toBinaryString(input.charAt(i));
            while (temp.length() < 8)
                temp = "0" + temp;
            s += temp;
        }

        return s;
    }

    private static int[] firstPhase(int[] keys, int p, int q) {
        int[] public_key;


        if (!checkInput(keys, p, q)) {
            System.err.println("Wrong values of p or q!");
            System.exit(1);
        }

        public_key = publicKeyGen(keys, p, q);
        saveKey(public_key, "public_key.txt");

        return public_key;
    }

    private static void saveKey(int[] data, String name) {
        try {
            FileWriter myWriter = new FileWriter(name);
            if (data.length > 0) {
                for (int i = 0; i < data.length - 1; ++i) {
                    myWriter.write(data[i] + ", ");
                }
                myWriter.write(String.valueOf(data[data.length - 1]));
            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private static int[] publicKeyGen(int[] keys, int p, int q) {
        int[] pkey = new int[keys.length];

        for (int i = 0; i < pkey.length; ++i) {
            pkey[i] = (keys[i] * p) % q;
        }
        return pkey;
    }

    private static boolean checkInput(int[] keys, int p, int q) {
        if (q % p == 0)
            return false;
        for (int i = 0; i < keys.length; ++i) {
            if (i > 0 && keys[i - 1] >= keys[i])
                return false;
            if (keys[i] >= q)
                return false;
        }
        return true;
    }

    private static int[] loadKeys(String key) {
        int[] keys = null;
        String[] s;
        try {
            File myObj = new File(key);
            if (!myObj.exists()) {
                System.err.println("File " + key + " does not exist!");
                System.exit(1);
            }
            Scanner myReader = new Scanner(myObj);

            s = myReader.nextLine().split(",");
            keys = new int[(s.length)];

            for (int i = 0; i < keys.length; ++i) {
                keys[i] = Integer.parseInt(s[i]);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return keys;
    }

    private static int[] extendedEukleid(int p1, int q1, int x) {
        int[] a = {0, 0};
        int[] b;
        int i = q1 % p1;
        if (i == x) {
            if (q1 == q)
                a[0] += 1;
            else if (q1 == p)
                a[1] += 1;
            else {
                b = extendedEukleid(p, q, q1);
                a[0] += b[0];
                a[1] += b[1];
            }
            if (p1 == p)
                a[1] += (-1) * ((q1 - x) / p1);
            else if (p1 == q)
                a[0] += 1;
            else {
                b = extendedEukleid(p, q, p1);
                a[0] += (-1) * ((q1 - x) / p1) * b[0];
                a[1] += (-1) * ((q1 - x) / p1) * b[1];
            }
            return a;
        }
        b = extendedEukleid(i, p1, x);
        a[0] += b[0];
        a[1] += b[1];
        return a;
    }
}
